///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
/// @file hello2.cpp
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Brooke Maeda <bmhm@hawaii.edu>
/// @brief  Lab 04c - HelloWorld - EE 205 - Spr 2021
/// @date   8 February 2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main(){

   std::cout << "Helllo World" << std::endl;

   return 0;

}




